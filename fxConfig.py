# Configuration script
# Author: Jason Buss
# Created: 2016-02-15
# Version: 1.0

import easygui
import os

def setConfig():
	#Setup configuration file
	mainpath = os.path.dirname(os.path.abspath(__file__))
	configpath = mainpath + "\\config"
	fullpath = configpath + "\\sql.config"
	fileexists = os.path.exists(fullpath)
	
	if fileexists == False:
		createConfigFile(fullpath)
	else:
		resp = easygui.ynbox("Config file already exists.  Would you like to recreate config file?", "Recreate File")
		if resp:
			createConfigFile(fullpath)
		
def createConfigFile(file):
	
	header = "# Holds project level variables"
	nickname = easygui.enterbox("Enter nickname", "Nickname", "Work", True)
	server = easygui.enterbox("Enter MSSQL Server instance", "MSSQL Instance", "", True)
	username = easygui.enterbox("Enter sql username", "Username", "", True)
	password = easygui.passwordbox("Enter Password", "Password")
	
		
	table = {header, 'location="' + nickname + '"', 'server="' + server + '"', 'username="' + username + '"', 'password="' + password + '"'}
	
	f = open(file, 'w')
	for item in table:
		f.write('{0}'.format(item)+"\n")
	
def main():
	# Launch setConfig function
	setConfig()
	
if __name__ == '__main__':
    main()