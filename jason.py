# Main working script
# Author: Jason Buss
# Created: 2016-02-09
# Version: 1.0.1

import jsys
import os
import pymssql as sql

path = os.path.dirname(jsys.__file__)

config = jsys.getVarFromFile(path + '\config\sql.config')

location = config.location

def executeSql(Database, SQL):
	# Run a sql statement against the database
	conn = sql.connect(config.server, config.username, config.password, Database)
	cursor = conn.cursor()
	cursor.execute(SQL)
	conn.commit()
	conn.close()
	
def runSql(Database, File):
	# Run a sql file against the Database
	
	file = open(File, 'r')
	sqlFileContents = file.read()
	file.close()

	conn = sql.connect(config.server, config.username, config.password, Database)
	cursor = conn.cursor()
	cursor.execute(sqlFileContents)
	conn.commit()